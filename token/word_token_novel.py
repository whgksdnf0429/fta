#!/usr/bin/env python
# coding: utf-8

# In[18]:


import re
from konlpy.tag import Okt


# In[19]:


f = open("../practice/novel_1.txt", "r")
text = f.read()


# In[20]:


# 한글과 공백을 제외한 모든 불용어 및 텍스트 제거
text = re.sub("[^ㄱ-힣^ ]", "", text)
# Open Korea Text 분석기를 이용하여 토큰화 진행
okt = Okt()
token = okt.morphs(text)


# In[21]:


print(token)

