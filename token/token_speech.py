#!/usr/bin/env python
# coding: utf-8

# In[1]:


from nltk.tokenize import RegexpTokenizer


# In[2]:


f = open("../practice/speech1.txt", "r")
text = f.read()


# In[3]:


# ..과 같은 비마침표 공백으로 전환
text = text.replace("..", " ")
# 정규식을 이용하여 마침표에 따른 토큰화 
tokenizer = RegexpTokenizer("[.?!]+", gaps = True)
token = tokenizer.tokenize(text)


# In[4]:


# 토큰의 맨 앞 공백 제거
for i in range(len(token)):
    if token[i][0] == " ":
        token[i] = token[i][1:]
# 토큰의 맨 뒤 공백 제거
for i in range(len(token)):
    if token[i][-1] == " ":
        token[i] = token[i][0:-1]
# 줄넘김 문자 제거
for i in range(len(token)):
        token[i] = token[i].replace("\n", "")
# 비마침표 공백 전환에 따른 공백 표시 복구
for i in range(len(token)):
        token[i] = token[i].replace("  ", " ")

print(token)

