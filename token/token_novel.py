#!/usr/bin/env python
# coding: utf-8

# In[218]:


from nltk.tokenize import RegexpTokenizer


# In[219]:


f = open("../practice/novel_1.txt", "r")
text = f.read()


# In[220]:


# ..과 같은 비마침표 공백으로 전환
text = re.sub("[.]{2,}", "", text)
# 특수문자, 줄넘김, 영어, 한자 제거
text = re.sub("[^ㄱ-힣^1-9. ]", "", text)
# 정규식을 이용하여 마침표에 따른 토큰화 
tokenizer = RegexpTokenizer("[.?!]+", gaps = True)
token = tokenizer.tokenize(text)


# In[221]:


# 토큰의 맨 앞 공백 제거
for i in range(len(token)):
    if token[i][0] == " ":
        token[i] = token[i][1:]
# 비마침표 공백 전환에 따른 공백 표시 복구
for i in range(len(token)):
        token[i] = token[i].replace("  ", " ")

print(token)

