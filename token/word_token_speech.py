#!/usr/bin/env python
# coding: utf-8

# In[11]:


import re
from konlpy.tag import Okt


# In[12]:


f = open("../practice/speech1.txt", "r")
text = f.read()


# In[18]:


# 한글과 숫자 공백을 제외한 모든 불용어 및 텍스트 제거
text = re.sub("[^ㄱ-힣^1-9 ]", "", text)
# Open Korea Text 분석기를 이용하여 토큰화 진행
okt = Okt()
token = okt.morphs(text)


# In[19]:


print(token)

