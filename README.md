# FTAP: Fill Text Analysis Program
Official Python implementation of Fill Text Analysis Program

**[조한울](mailto:whgksdnf12155@naver.com), 김병훈, 이제완**
 
Department of Bigdata Engineering, Soonchunhyang University

## Overview
불의의 사고나 인터넷상의 오류 등으로 인한 데이터의 텍스트 누락,  
음성인식에서의 노이즈로 인한 텍스트의 불확실성과 같은 **문제**를  
  
**카테고리별로 나누어 텍스트의 맥락, 누락된 문장 이전의 단어를 통해  
문장을 예측하고 생성함으로써 해결할 수 있는 프로그램을 개발.**

Text missing, of the data due to an accident or an error on the Internet, etc.  
Voice recognition and uncertainty of the noise caused by the text in the same problem.  
Missing through word of the previous sentence, the context of text divided by category.  
Create a program that can anticipate and solve this by creating a sentence.


## Updates
**18 Sep, 2019**: Start Project  
**10 Dec, 2019**: Finish Project


## Getting started
### Install dependencies
#### Requirements
- Python=3.6.0
- PyQT5>=5.13.2
- PyMySQL>=3.4.2
- konlpy>=0.5.2
```
pip install pyqt5
pip install pymysql
pip install konlpy
```

### Training
전처리, 구축, 훈련을 위해 필요한 코드는 해당 프로젝트 안에 카테고리별로 저장되어 있습니다.  
  
The code required for pre-processing, construction, and training is stored within the project by category


### How to use
해당 프로젝트에서 기본적으로 GUI프로그램을 제공합니다.  
하지만 해당 프로그램을 직접 구축하고 싶은 사람들을 위해 간략한 절차를 설명해드리겠습니다.  
  
The project provides a GUI program by default.  
But I'll give you a brief procedure for those who want to build their own programs.
  
**1.  자신이 원하는 카테고리의 데이터 수집  
2.  데이터베이스를 구축하여 저장  
3.  데이터를 가져와서 취합한 후 전처리 진행  
4.  마르코프 체인을 이용한 예측 및 생성 모델 구축  
5.  GUI프로그램의 레이아웃을 짜고 각 버튼을 모델과 연결**  

  
위 과정의 모든 내용은 해당 프로젝트 파일에 모두 구현되어 있습니다.  
  
All of the above are implemented in the project files.




## Contact & Feedback
조한울 Tel)010-2282-4957 / E-mail)whgksdnf12155@naver.com  
김병훈 Tel)010-5015-5129 / E-mail)qudgns5129@gmail.com  
이제완 Tel)010-3680-7578 / E-mail)lob1010@naver.com


## License
```
본 프로젝트에서 사용한 모든 권한은 FTA팀이 소유하고 있습니다.  
하지만 예외로 띄어쓰기 프로그램을 대체할 수 있는 네이버 맞춤법 검사기 오픈 소스를 이용하였으므로  
해당 라이센스는 네이버에 귀속되어 있습니다.  
이에 따라 사본을 획득하는 모든 사람에게 네이버 맞춤법 검사기를 제외한 모든 권한을 무료로 부여합니다.  
  
All rights used in this project are owned by the FTA team.  
However, as an exception, we used Naver's custom-made inspection machine open source, which can replace the space writing program.  
The license is assigned to Naver.  
As a result, all rights except the Naver Spell Checker will be granted free of charge to anyone who obtains a copy.
```
