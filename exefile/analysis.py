#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.layers import LSTM
from keras.optimizers import RMSprop
from keras.utils.data_utils import get_file
import numpy as np
import random, sys 


def analys(file):
    fp = open(file, "r", encoding="utf-8")
    text = fp.read()
    text = text.replace("…", "")
    chars = sorted(list(set(text)))
    char_indices = dict((c, i) for i, c in enumerate(chars)) # 문자 → ID
    indices_char = dict((i, c) for i, c in enumerate(chars)) # ID → 문자
    
    # 텍스트를 maxlen의 크기만큼 분할하고 다음 문자 등록
    maxlen = 20
    step = 3
    sentences = []
    next_chars = []
    
    for i in range(0, len(text) - maxlen, step):
        sentences.append(text[i: i + maxlen])
        next_chars.append(text[i + maxlen])
    X = np.zeros((len(sentences), maxlen, len(chars)), dtype=np.bool)
    y = np.zeros((len(sentences), len(chars)), dtype=np.bool)
    
    for i, sentence in enumerate(sentences):
        for t, char in enumerate(sentence):
            X[i, t, char_indices[char]] = 1
        y[i, char_indices[next_chars[i]]] = 1
        
    model = Sequential()
    model.add(LSTM(128, input_shape=(maxlen, len(chars))))
    model.add(Dense(len(chars)))
    model.add(Activation('softmax'))
    optimizer = RMSprop(lr=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=optimizer)
    
    # 배열로부터 문자열 후보 뽑기
    def sample(preds, temperature=1.0):
        preds = np.asarray(preds).astype('float64')
        preds = np.log(preds) / temperature
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        return np.argmax(probas)
    
    #학습과 테스트를 반복함
    for iteration in range(1, 60):
        print()
        print('-' * 50)
        print('반복 =', iteration)
        model.fit(X, y, batch_size=128, nb_epoch=1) 
        for iteration in range(1, 60):
            print()
            print('-' * 50)
            print('반복 =', iteration)
            model.fit(X, y, batch_size=128, nb_epoch=1) # 
            # 임의의 시작 텍스트 선정
            start_index = random.randint(0, len(text) - maxlen - 1)
            # 다양성별 다양한 문장 생성
            for diversity in [0.2, 0.5, 1.0, 1.2]:
                print()
                print('--- 다양성 = ', diversity)
                generated = ''
                sentence = text[start_index: start_index + maxlen]
                generated += sentence
                print('--- 시드 = "' + sentence + '"')
                sys.stdout.write(generated)
                # 시드 기반 텍스트 생성
                for i in range(400):
                    x = np.zeros((1, maxlen, len(chars)))
                    for t, char in enumerate(sentence):
                        x[0, t, char_indices[char]] = 1.
                    # 다음 문자 예측
                    preds = model.predict(x, verbose=0)[0]
                    next_index = sample(preds, diversity)
                    next_char = indices_char[next_index]
                    # 출력
                    generated += next_char
                    sentence = sentence[1:] + next_char
                    sys.stdout.write(next_char)
                    sys.stdout.flush()

